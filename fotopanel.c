/**
* \mainpage
* \par Sterowanie panelem fotowoltanicznym
* \author Radosław Jezierski, Karol Szałecki
* \date 2014.06.09
* \version 1.0

*/

/**
 *  podłączenie wyświetlacza LCD
 * The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 
 */

#include <LiquidCrystal.h>
#include <Servo.h> 

/// Inicjalizacja wyświetlacza
/// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
Servo myservo;  
int potpin = 0;         /// numer pinu wejścia analogowego do którego dołączony jest panel
int val=0;              /// zmienna do której wpisywane jest napięcie z panelu
int tabPanel[180];		/// Tablica zmiennych z odczytanymi napięciami
int tabPolozenie[180];	/// Tablica zmiennych z odczytanymi pozycjami
int pos = 0;			/// zmienna przechowująca położenie.
int maksymalna=0;		/// zmienna przechowująca maksymalna wartość napiecia z panelu
int polozenie=0;		// zmienna przechowująca położenie panelu przy maksymalnym napięciu
int czaspoprzedni=0;

/// Pętla wykonywana jeden raz po uruchomieniu mikroproesora
void setup() {
   /// Ustawiane są tutaj następujące parametry:
lcd.begin(16, 2);    /// - ustawienie wielkości wyświetlacza
myservo.attach(10);  /// - ustawienie pinu do którego podłączony jest servomechanizm
myservo.write(0);    /// - ustawienie serwomechanizmu w pozycji zerowej
/// Zmienne występujace w funkcji:
maksymalna=0;        /// - zmienna przechowująca maksymalna wartość napiecia z panelu
polozenie=0;         /// - zmienna przechowująca położenie panelu przy maksymalnym napięciu
tabPanel[pos]=0;     /// - tablica z wartościami napięcia
tabPolozenie[pos]=0; /// - tablica z wartościami położenia
delay(2000);         /// Następnie program odczekuje 2 sekundy. 
/// Pętla zczytująca wartości napięć i ustalenie położenia panelu przy maksymalnym napięciu
  for(pos = 1; pos < 180; pos += 1)  
  {   
    val = analogRead(potpin); ///Następuje tutaj odczytanie i przypisanie napięcia do zmiennej
    lcd.setCursor(0, 0);
    lcd.print("Akt:");
    lcd.setCursor(5, 0);
    lcd.print("V:");  
    lcd.setCursor(7, 0);
    lcd.print(val);  
    lcd.setCursor(11, 0);
    lcd.print("P:");  
    lcd.setCursor(13, 0);
    lcd.print(pos); 
    tabPanel[pos]=val;
    tabPolozenie[pos]=pos;
    
if(tabPanel[pos]>maksymalna && tabPanel[pos]<tabPanel[pos-1]+60) /// Pętla ustalająca czy obecnee napięcia jest większe od maksymalnego i usuwająca błędne wartości. 
{                                                               /// Odczyty z panelu wynikające z niewiadomych przyczyn, a działa to tak że kolejna próbka nie może być większa od poprzedniej o 60
  maksymalna=tabPanel[pos];
  polozenie=tabPolozenie[pos];  
}

   lcd.setCursor(0, 1);
    lcd.print("Max:");
    lcd.setCursor(5, 1);
    lcd.print("V:");  
    lcd.setCursor(7, 1);
    lcd.print(maksymalna);  
    lcd.setCursor(11, 1);
    lcd.print("P:");  
    lcd.setCursor(13, 1);
    lcd.print(polozenie); 
  
    myservo.write(pos);                       // ustawienie servomechanizmu w danej pozycji   
    delay(30); 
        
  } 
  
  for(pos = 180; pos>=polozenie; pos-=1)      // pętla przesuwająca panel do pozycji ustalonej jako maksymalna
  {    
    myservo.write(pos);               
    delay(30);                                // odczekanie 30 milisekund by móc wykonać następny ruch
        
  } 
}
/// Pętla skanująca co 20 sekund otoczenie maksymalnego napięcia, w celu dostosowania nowej optymalnej wartości
void loop() {
/**
* Pętla while czekająca 20 sekund w rzeczywistości około 30 minut by odczytać ponownie napięcie.
* Jest to po to by poruszyć panel w kierunku przsuwającego się słońca . W celu zaoszczędzenia energi porzuszanie panelu nie jest wykonywane od 0 do 180 stopni, a tylko +-40 stopni od ustawienia bieżącego.  
  */
  czaspoprzedni=millis()/1000;                        
  while(((millis()/1000)-czaspoprzedni)<20)
  {
    //myservo.write(polozenie);
    //val = analogRead(potpin);
  }
  //-------------------------------------------------------------
  
   for(pos = polozenie; pos>=polozenie-40; pos-=1)   /// przesuniecie do pozycji -40 stopni od pozycji maksymalnej   
  {    
    myservo.write(pos);               
    delay(30); 
       
  } 
  
 //---------------------------------------------------------------- 
 for(pos = 1; pos < 180; pos += 1)  /// zerowanie tablicy 
  {
    tabPanel[pos]=0;
    tabPolozenie[pos]=0;
  } 
    maksymalna=0;
  for(pos = polozenie-40; pos < polozenie+40; pos += 1)    /// ustalenie pozycji w której napięcie jest maksymalnew tak jak to w pętli Setup od -40 stopni do +40 stopni od pozycji maksymalnej 
  {  
    val = analogRead(potpin);
    lcd.setCursor(0, 0);
    lcd.print("Akt:");
    lcd.setCursor(5, 0);
    lcd.print("V:");  
    lcd.setCursor(7, 0);
    lcd.print(val);  
    lcd.setCursor(11, 0);
    lcd.print("P:");  
    lcd.setCursor(13, 0);
    lcd.print(pos); 
    tabPanel[pos]=val;
    tabPolozenie[pos]=pos;
   
if(tabPanel[pos]>maksymalna && tabPanel[pos]<tabPanel[pos-1]+60)
{
  maksymalna=tabPanel[pos];
  polozenie=tabPolozenie[pos];  
}
  lcd.setCursor(0, 1);
    lcd.print("Max:");
    lcd.setCursor(5, 1);
    lcd.print("V:");  
    lcd.setCursor(7, 1);
    lcd.print(maksymalna);  
    lcd.setCursor(11, 1);
    lcd.print("P:");  
    lcd.setCursor(13, 1);
    lcd.print(polozenie);
  
    myservo.write(pos);               
    delay(30);  
  } 
 
 //---------------------------------------------------------------
  for(pos = polozenie+40; pos>=polozenie; pos-=1)      /// łagodne przesunięcie do pozycji maksymalnej
  {          
    myservo.write(pos);               
    delay(30);          
  }
  }
